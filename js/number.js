import { createEl } from "./game.js";
import { game } from "./script.js";

export default class Numb {
    constructor(cell, pow = 1) {

        // number main properties
        this.id     = null;
        this.dom    = null;
        this.cell   = cell;
        this.pow    = pow;
        this.start  = 2;
        this.colors = [
            { bg: '#eee4da', color: '#776e65' },
            { bg: '#eee1c9', color: '#776e65' },
            { bg: '#f3b27a', color: '#ffffff' },
            { bg: '#f69664', color: '#ffffff' },
            { bg: '#f77c5f', color: '#ffffff' },
            { bg: '#f75f3b', color: '#ffffff' },
        ];

        // transform properties
        this.combined  = false;
        this.skipped   = false;
        this.isUpgrade = false;

        // the first called methods
        this.create();
    }

    create() {
        game.numHistories += 1;
        this.id = game.numHistories;
        this.dom = createEl('div', {'data-id': this.id}, ['number'], this.num);
        game.app.append(this.dom);
        if (!this.cell) this.fillCell();
        this.setAppearance();
    }

    fillCell() {
        this.cell = game.cells.filter(cell => !game.nums.filter(num => num.cell).map(num => num.cell.id).includes(cell.id))[Math.floor(Math.random() * (game.cells.length - 1))];
    }

    get num() {
        return Math.pow(this.start, this.pow);
    }

    move(dir, skip) {

        // call the move method
        ({
            left   : (skip) => this.moveLeft(skip),
            top    : (skip) => this.moveUp(skip),
            right  : (skip) => this.moveRight(skip),
            bottom : (skip) => this.moveDown(skip),
        })[dir](skip);

        this.setPosition();
        this.checkAfter();
    }

    moveUp(skip) {
        this.cell = game.cells.find(cell => cell.row === skip + 1 && cell.col === this.cell.col);
    }

    moveRight(skip) {
        this.cell = game.cells.find(cell => cell.row === this.cell.row && cell.col === game.settings.colLen - skip);
    }

    moveDown(skip) {
        this.cell = game.cells.find(cell => cell.row === game.settings.rowLen - skip && cell.col === this.cell.col);
    }

    moveLeft(skip) {
        this.cell = game.cells.find(cell => cell.row === this.cell.row && cell.col === skip + 1);
    }

    setAppearance() {
        this.setPosition();
        this.setSize();
        this.setColor();
    }

    setPosition() {
        this.dom.style.left   = `${this.cell.dom.offsetLeft}px`;
        this.dom.style.top    = `${this.cell.dom.offsetTop}px`;
    }

    setSize() {
        this.dom.style.width  = `${this.cell.dom.offsetWidth}px`;
        this.dom.style.height = `${this.cell.dom.offsetHeight}px`;
    }

    setColor() {
        this.dom.style.background = `${this.colors[this.pow - 1] ? this.colors[this.pow - 1].bg : this.colors[this.colors.length - 1].bg}`;
        this.dom.style.color = `${this.colors[this.pow - 1] ? this.colors[this.pow - 1].color : this.colors[this.colors.length - 1].color}`;
    }

    checkAfter() {
        if (this.combined) setTimeout(() => this.destroy(), game.settings.transitionDuration);

        if (this.isUpgrade) {
            this.upgrade();
            this.isUpgrade = false;
            game.settings.board.score.increase(this.num);
        }
    }

    upgrade() {
        this.pow += 1;
        this.dom.innerHTML = this.num;
        this.setAppearance();
    }

    destroy() {
        this.dom.remove();
        game.nums.splice(game.nums.findIndex(num => num.id === this.id), 1);
    }

}