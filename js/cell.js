import { createEl } from "./game.js";
import { game } from "./script.js";

export default class Cell {
    constructor(row, col) {

        // cell main properties
        this.dom = null;
        this.row = row;
        this.col = col;
        this.id  = game.cells.length + 1;

        // first called methods
        this.create();

    }

    create() {
        this.dom = createEl('div', { 'data-row': this.row, 'data-col': this.col }, [ 'cell' ]);
        game.app.append(this.dom);
    }

    get num() {
        return game.nums.find(num => num.cell.id === this.id);
    }
}