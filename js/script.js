import Game from './game.js';

export const game = new Game();

window.onload = () => game.render();