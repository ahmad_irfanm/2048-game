import Cell from "./cell.js";
import Numb from "./number.js";
import Board from "./board.js";

export default class Game {
    constructor() {

        // game main properties
        this.app   = getId('app');
        this.cells = [];
        this.nums  = [];

        // game settings
        this.settings = {
            rowLen: 4,
            colLen: 4,
            transitionDuration: 100,
            board: new Board(),
        };

        // ID increment of numbers
        this.numHistories = 0;

    }

    render() {
        this.cleanUp();
        this.create();
        this.listen();
    }

    cleanUp() {
        this.cells = [];
        this.nums  = [];
        this.app.innerHTML = '';
    }

    create() {
        this.createCells();
        this.createNumbers();
    }

    createCells() {
        this.app.style.gridTemplateColumns = `repeat(${this.settings.colLen}, 1fr)`;
        this.app.style.gridTemplateRows    = `repeat(${this.settings.rowLen}, 1fr)`;
        for(let y = 1; y <= this.settings.rowLen; y++) for(let x = 1; x <= this.settings.colLen; x++) this.cells.push( new Cell(y, x) );
    }

    createNumbers() {
        this.newNumber(2);
    }

    listen() {
        window.addEventListener('keydown', (e) => {
            let key = e.keyCode;

            switch (key) {
                case 37:
                    this.matching('left');
                    break;
                case 38:
                    this.matching('top');
                    break;
                case 39:
                    this.matching('right');
                    break;
                case 40:
                    this.matching('bottom');
                    break;
            }
        });
    }

    newNumber(n = 1) {
        for (let i = 0; i < n; i++) {
            let cellsAvailable = this.cells.filter(cell => !cell.num);
            if (cellsAvailable.length === 0) { this.gameOver(); return; }
            let randIndex = Math.floor(Math.random() * (cellsAvailable.length - 1));
            let cell = cellsAvailable[randIndex];
            this.nums.push(new Numb(cell, 1));
        }
    }

    matching(dir) {

        let horizontal = dir === 'left'  || dir === 'right',
            opposite   = dir === 'right' || dir === 'bottom';

        let len = horizontal ? this.settings.rowLen : this.settings.colLen, nums = [], indexBefore;

        for (let i = 1; i <= len; i++) {

            nums = this.nums.filter(num => (horizontal ? num.cell.row : num.cell.col) === i);

            if (opposite) nums.sort((a, b) => !horizontal ? b.cell.row - a.cell.row : b.cell.col - a.cell.col );
            else nums.sort((a, b) => !horizontal ? a.cell.row - b.cell.row : a.cell.col - b.cell.col );

            nums.forEach((num, i) => {
                indexBefore = i - 1;
                if (i > 0 && !nums[indexBefore].combined && nums[indexBefore].pow === num.pow) {
                    num.combined = true;
                    num.skipped  = indexBefore;
                    nums[indexBefore].isUpgrade = true;
                }
            });

            // move all of combined numbers
            nums.filter(num => num.combined).forEach((num) => num.move(dir, num.skipped));

            // move all of numbers
            nums.filter(num => !num.combined).forEach((num, i) => num.move(dir, i));

        }

        setTimeout(() => this.newNumber(1), this.settings.transitionDuration);

    }

    gameOver() {
        this.settings.board.highscore.store();
        location.reload();
    }
}

export const createEl = (tag, attrs = {}, classes = [], html = false) => {
    tag = document.createElement(tag);
    Object.keys(attrs).forEach(key => { tag.setAttribute(key, attrs[key]); });
    classes.forEach(_class => tag.classList.add(_class));
    if (html) tag.innerHTML = html;
    return tag;
}

export const getId = (id) => document.getElementById(id);