import { getId } from "./game.js";

export default class Board {
    constructor() {

        // score properties
        this.score = {
            dom: getId('score'),
            num: 0,
            increase: (addition) => {
                this.score.num += addition;
                this.score.dom.innerHTML = this.score.num;
            }
        };

        // high score properties
        this.highscore = {
            dom: getId('best'),
            num: 0,
            store: () => {
                this.highscore.num = this.score.num > this.highscore.num ? this.score.num : this.highscore.num;
                localStorage.setItem('best', this.highscore.num);
            },
            set: () => {
                if (localStorage.getItem('best')) {
                    this.highscore.num = parseInt(localStorage.getItem('best'));
                    this.highscore.dom.innerHTML = this.highscore.num;
                }
            }
        };

        // first called method
        this.create();

    }
    create() {

        // set init score
        this.score.increase(0);

        // set init high score
        this.highscore.set();

    }
}